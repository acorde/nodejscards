const express = require("express");
const router = express.Router();
const db = require('./../db/models');

router.post("/customers", async (req, res) => {
    var _customers = req.body;     
    var _address = req.body.address
    //console.log("Conteúdo da variável _address: ", _address)
    await db.customers.create(_customers).then((dadosUsuario) => { 
        _address.customersId = dadosUsuario.id;
         db.address.create(_address).then((dadosAddress) => {           
            var Customers = (dadosUsuario).toJSON();       
            Customers.address = (dadosAddress).toJSON();
            
            return ret = res.json({
                mensagem: "Customers cadastrado com sucesso!",
                Customers,                
            });
        });

    }).catch(() => {
        return res.json({
            mensagem: "Erro: Customers não cadastrado!"
        });
    });    
});

router.get("/customers", async (req, res) => {
    const { page = 1 } = req.query;									//Calcular paginação.
		const limit = 10;                                               //Calcular paginação.
		var lastPage = 1;                                               //Calcular paginação.
		const countUser = await db.customers.count();                       //Calcular paginação.
		if(countUser !== 0){                                            //Calcular paginação.
			lastPage = Math.ceil(countUser / limit);                    //Calcular paginação.
		}else{                                                          //Calcular paginação.
			return res.status(400).json({                               //Calcular paginação.
				mensagem: "Erro: Nenhum customers encontrado!"            //Calcular paginação.
			});                                                         //Calcular paginação.
		}                                                               //Calcular paginação.
    //
    const customers = await db.customers.findAll({
        attributes: ['id','CodeHtml','CodeInternal','CnpjParameter','CNPJConsulted','NumberRegistration','NameBusiness','RegistrationStage','SupplierName'],
        order: [['id','DESC']],
        //Calcular a partir de qual registro deve retornar 
        //e o limite de registros
        offset: Number((page * limit) - limit),						//Calcular paginação
        limit: limit                                                //Calcular paginação
    });
    //

    if(customers){
        var pagination = {											//Retornar dados paginados.
            path: "/customers",                                         //Retornar dados paginados.
            page: page,                                             //Retornar dados paginados.
            prev_page_url: page - 1 >= 1 ? page - 1 : page,         //Retornar dados paginados.
            next_page_url: Number(page) + Number(1) > lastPage      //Retornar dados paginados.
                            ? lastPage : Number(page) + Number(1),  //Retornar dados paginados.
            lastPage: lastPage,                                     //Retornar dados paginados.
            total: countUser                                        //Retornar dados paginados.
        }                                                           //Retornar dados paginados.
        return res.json({
            users,
            pagination 												//Retornar dados paginados.
        });
    }else{
        return res.status(400).json({
            mensagem: "Erro: Nenhum usuário encontrado!"
        });
    }

});

module.exports = router;