'use strict';
const {  Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class customers extends Model {
    static associate(models) {
      this.hasMany(models.address);
    }
  }
  customers.init({
    cnpjParameter: DataTypes.STRING,
    nameBusiness: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'customers'
  });
  return customers;
};