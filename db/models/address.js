'use strict';
const { Model } = require('sequelize');
const { FOREIGNKEYS } = require('sequelize/lib/query-types');
module.exports = (sequelize, DataTypes) => {
  class address extends Model {
    static associate(models) {
      this.belongsTo(models.customers, { foreignKey: 'customersId' });
    }
  }
  address.init({
    ufParameter: DataTypes.STRING,
    logradouro: DataTypes.STRING,
    numero: DataTypes.STRING,
    customersId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'address',
    freezeTableName: true,
  });
  return address;
};